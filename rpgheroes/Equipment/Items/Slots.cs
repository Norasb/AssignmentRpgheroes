﻿
namespace rpgheroes.Equipment.Items {

    /// <summary>
    /// Slots for where weapon and armor can be equipped
    /// </summary>
    public enum Slots {
        Weapon,
        Head,
        Body,
        Legs
    }
}
