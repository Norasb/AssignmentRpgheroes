﻿namespace rpgheroes.Equipment.Items {

    /// <summary>
    /// Abstract class Item - With subclasses: Weapon, Armor
    /// </summary>
    public abstract class Item {

        //Fields
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slots Slot { get; set; }

        //Constructor
        public Item(string name, int requiredLevel, Slots slot) {
            Name = name;
            RequiredLevel = requiredLevel;
            Slot = slot;
        }
    }
}
