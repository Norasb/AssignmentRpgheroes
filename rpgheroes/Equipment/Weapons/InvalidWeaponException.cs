﻿namespace rpgheroes.Equipment.Weapons {

    /// <summary>
    /// Exception to be thrown when hero try to equip wrong weapontype or level requirement is too high
    /// </summary>
    public class InvalidWeaponException : Exception {
        public override string Message => "Cannot equip this weapon, either wrong type or level requirement is too high";
    }
}
