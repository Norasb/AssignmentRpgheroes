﻿using rpgheroes.Equipment.Items;


/// <summary>
/// Weapon class to create Weapon that a hero can equip - Subclass of Item
/// </summary>
namespace rpgheroes.Equipment.Weapons {
    public class Weapon : Item {

        //Fields
        public WeaponTypes TypeOfWeapon { get; set; }
        public int WeaponDamage { get; set; }

        //Constructor
        public Weapon(string name, int requiredLevel, Slots slot, WeaponTypes typeOfWeapon, int weaponDamage) : base(name, requiredLevel, slot) {
            TypeOfWeapon = typeOfWeapon;
            WeaponDamage = weaponDamage;
        }
        

    }

    
}
