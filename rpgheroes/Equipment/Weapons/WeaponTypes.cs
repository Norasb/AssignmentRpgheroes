﻿

namespace rpgheroes.Equipment.Weapons {

    /// <summary>
    /// Weapontypes different heroes can equip
    /// </summary>
    public enum WeaponTypes {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }
}
