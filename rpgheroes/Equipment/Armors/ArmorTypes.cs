﻿

namespace rpgheroes.Equipment.Armors {
    /// <summary>
    /// Available armortypes different heroes can equip
    /// </summary>
    public enum ArmorTypes {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
