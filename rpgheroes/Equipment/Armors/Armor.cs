﻿using rpgheroes.Equipment.Items;
using rpgheroes.Attribute;

/// <summary>
/// Armor class to create Armor that a hero can equip - Subclass of Item
/// </summary>
namespace rpgheroes.Equipment.Armors {
    public class Armor : Item {

        //Fields
        public ArmorTypes TypeOfArmor { get; set; }
        public HeroAttribute ArmorAttribute { get; set; }

        //Constructor
        public Armor(string name, int requiredLevel, Slots slot, ArmorTypes typeOfArmor, int strength, int dexterity, int intelligence) : base(name, requiredLevel, slot) {
            TypeOfArmor = typeOfArmor;
            ArmorAttribute = new HeroAttribute(strength, dexterity, intelligence);
        }  
    }


    
}
