﻿namespace rpgheroes.Equipment.Armors {

    /// <summary>
    /// Exception to be thrown when hero try to equip wrong armortype or level requirement is too high
    /// </summary>
    public class InvalidArmorException : Exception {
        public override string Message => "Cannot equip this armor, either wrong armor type or level requirement is too high";
    }
}
