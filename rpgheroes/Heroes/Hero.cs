﻿using System.Text;
using rpgheroes.Equipment.Items;
using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Attribute;

namespace rpgheroes.Heroes {

    /// <summary>
    /// Abstract class Hero - Subclasses: Mage, Ranger, Rogue, Warrior
    /// </summary>
    public abstract class Hero {

        //Fields
        public string Name { get; set; }
        public int Level { get; set; }
        public HeroAttribute LevelAttributes { get; set; }
        public IDictionary<Slots, Item> Equipment { get; set; }
        public List<WeaponTypes> ValidWeaponTypes { get; set; }
        public List<ArmorTypes> ValidArmorTypes { get; set; }

        //Constructor
        public Hero(string name) {
            Name = name;
            Level = 1;
            LevelAttributes = new HeroAttribute(0, 0, 0);
            Equipment = new Dictionary<Slots, Item>();
            Equipment.Add(new KeyValuePair<Slots, Item>(Slots.Head, null));
            Equipment.Add(new KeyValuePair<Slots, Item>(Slots.Body, null));
            Equipment.Add(new KeyValuePair<Slots, Item>(Slots.Legs, null));
            Equipment.Add(new KeyValuePair<Slots, Item>(Slots.Weapon, null));
            ValidWeaponTypes = new List<WeaponTypes>();
            ValidArmorTypes = new List<ArmorTypes>();
        }

        //Methods
        public virtual void LevelUp() {
            Level++;
        }

        public void EquipArmor(Armor armor) {
            if (armor.RequiredLevel <= Level && ValidArmorTypes.Contains(armor.TypeOfArmor)) {
                Equipment[armor.Slot] = armor;
            } else {
                throw new InvalidArmorException();
            }
            
        }

        public void EquipWeapon(Weapon weapon) {
            if (weapon.RequiredLevel <= Level && ValidWeaponTypes.Contains(weapon.TypeOfWeapon)) {
                Equipment[weapon.Slot] = weapon;
            } else {
                throw new InvalidWeaponException();
            }
        }


        public abstract double Damage();

        public HeroAttribute TotalAttributes() {
            HeroAttribute totalAttributes = new HeroAttribute(LevelAttributes.Strength, LevelAttributes.Dexterity, LevelAttributes.Intelligence);
            if (Equipment[Slots.Head] != null) {
                Armor headArmor = (Armor)Equipment[Slots.Head];
                totalAttributes.Strength += headArmor.ArmorAttribute.Strength;
                totalAttributes.Dexterity += headArmor.ArmorAttribute.Dexterity;
                totalAttributes.Intelligence += headArmor.ArmorAttribute.Intelligence;
            }
            if (Equipment[Slots.Body] != null) {
                Armor bodyArmor = (Armor)Equipment[Slots.Body];
                totalAttributes.Strength += bodyArmor.ArmorAttribute.Strength;
                totalAttributes.Dexterity += bodyArmor.ArmorAttribute.Dexterity;
                totalAttributes.Intelligence += bodyArmor.ArmorAttribute.Intelligence;
            }
            if (Equipment[Slots.Legs] != null) {
                Armor legArmor = (Armor)Equipment[Slots.Legs];
                totalAttributes.Strength += legArmor.ArmorAttribute.Strength;
                totalAttributes.Dexterity += legArmor.ArmorAttribute.Dexterity;
                totalAttributes.Intelligence += legArmor.ArmorAttribute.Intelligence;
            }
            return totalAttributes;
        }

        public string Display() {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name: " + Name);
            sb.AppendLine("Herotype: " + GetType().Name);
            sb.AppendLine("Level: " + Level);
            sb.AppendLine("Total Attributes: \n- Strength: " + TotalAttributes().Strength + "\n- Dexterity: " + TotalAttributes().Dexterity + "\n- Intelligence: " + TotalAttributes().Intelligence);
            sb.AppendLine("Damage: " + Damage());

            return sb.ToString();
        }
    }
}
