﻿using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Items;

namespace rpgheroes.Heroes {
    /// <summary>
    /// Mage class to create a Mage hero - Subclass of Hero
    /// </summary>
    public class Mage : Hero {

        //Constructor
        public Mage(string name) : base(name) {
            LevelAttributes = new HeroAttribute(1, 1, 8);
            ValidArmorTypes.Add(ArmorTypes.Cloth);
            ValidWeaponTypes.Add(WeaponTypes.Staff);
            ValidWeaponTypes.Add(WeaponTypes.Wand);
        }

        //Methods
        public override double Damage() {
            double damage = 0f;
            if (Equipment[Slots.Weapon] == null) {
                damage = 1 * (1 + (TotalAttributes().Intelligence / 100D));
            } else {
                Weapon weapon = (Weapon)Equipment[Slots.Weapon];
                damage = weapon.WeaponDamage * (1 + (TotalAttributes().Intelligence / 100D));
            }
            return damage;
        }

        public override void LevelUp() {
            base.LevelUp();
            LevelAttributes.increaseAttributes(1, 1, 5);
        }
    }
}
