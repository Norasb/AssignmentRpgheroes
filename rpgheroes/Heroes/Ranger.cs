﻿using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Items;

/// <summary>
/// Ranger class to create a Ranger hero - Subclass of Hero
/// </summary>
namespace rpgheroes.Heroes {
    public class Ranger : Hero {

        //Constrcutor
        public Ranger(string name) : base(name) {
            LevelAttributes = new HeroAttribute(1, 7, 1);
            ValidArmorTypes.Add(ArmorTypes.Leather);
            ValidArmorTypes.Add(ArmorTypes.Mail);
            ValidWeaponTypes.Add(WeaponTypes.Bow);
        }

        //Methods
        public override double Damage() {
            double damage;
            if (Equipment[Slots.Weapon] == null) {
                damage = (1 * (1 + (TotalAttributes().Dexterity / 100D)));
            } else {
                Weapon weapon = (Weapon)Equipment[Slots.Weapon];
                damage = (weapon.WeaponDamage * (1 + (TotalAttributes().Dexterity / 100D)));
            }
            return damage;
        }

        public override void LevelUp() {
            base.LevelUp();
            LevelAttributes.increaseAttributes(1, 5, 1);
        }
    }
}
