﻿using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Items;

namespace rpgheroes.Heroes {

    /// <summary>
    /// Rogue class to create a Rogue hero - Subclass of Hero
    /// </summary>
    public class Rogue : Hero {

        //Constructor
        public Rogue(string name) : base(name) {
            LevelAttributes = new HeroAttribute(2, 6, 1);
            ValidArmorTypes.Add(ArmorTypes.Leather);
            ValidArmorTypes.Add(ArmorTypes.Mail);
            ValidWeaponTypes.Add(WeaponTypes.Dagger);
            ValidWeaponTypes.Add(WeaponTypes.Sword);
        }


        //Methods
        public override double Damage() {
            double damage = 0f;
            if (Equipment[Slots.Weapon] == null) {
                damage = 1 * (1 + (TotalAttributes().Dexterity / 100D));
            } else {
                Weapon weapon = (Weapon)Equipment[Slots.Weapon];
                damage = weapon.WeaponDamage * (1 + (TotalAttributes().Dexterity / 100D));
            }
            return damage;
        }

        public override void LevelUp() {
            base.LevelUp();
            LevelAttributes.increaseAttributes(1, 4, 1);
        }
    }
}
