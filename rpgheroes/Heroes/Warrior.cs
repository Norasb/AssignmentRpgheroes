﻿using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Items;

namespace rpgheroes.Heroes {

    /// <summary>
    /// Warrior class to create a Warrior hero - Subclass of Hero
    /// </summary>
    public class Warrior : Hero {

        //Constructor
        public Warrior(string name) : base(name) {
            LevelAttributes = new HeroAttribute(5, 2, 1);
            ValidArmorTypes.Add(ArmorTypes.Mail);
            ValidArmorTypes.Add(ArmorTypes.Plate);
            ValidWeaponTypes.Add(WeaponTypes.Axe);
            ValidWeaponTypes.Add(WeaponTypes.Hammer);
            ValidWeaponTypes.Add(WeaponTypes.Sword);
        }

        //Methods
        public override double Damage() {
            double damage = 0f;
            if (Equipment[Slots.Weapon] == null) {
                damage = 1 * (1 + (TotalAttributes().Strength / 100D));
            } else {
                Weapon weapon = (Weapon)Equipment[Slots.Weapon];
                damage = weapon.WeaponDamage * (1 + (TotalAttributes().Strength / 100D));
            }
            return damage;
        }

        public override void LevelUp() {
            base.LevelUp();
            LevelAttributes.increaseAttributes(3, 2, 1);
        }
    }
}
