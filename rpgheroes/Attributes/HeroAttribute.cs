﻿namespace rpgheroes.Attribute {

    /// <summary>
    /// Heroattribute to create for a hero to have different strength, dexterity and intelligence values
    /// </summary>
    public class HeroAttribute {

        //Fields
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        //Constructor
        public HeroAttribute(int strength, int dexterity, int intelligence) {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }


        //Methods
        public void increaseAttributes(int strength, int dexterity, int intelligence) {
            Strength += strength;
            Dexterity += dexterity;
            Intelligence += intelligence;
        }
    }
}
