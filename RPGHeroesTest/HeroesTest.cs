using Xunit;
using System.Text;
using rpgheroes.Heroes;
using FluentAssertions;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Items;

namespace RPGHeroesTest {
    public class HeroesTest {
        [Fact]
        public void Mage_CreateMageWithName_ShouldReturnCorrectName() {
            //Should test for name, level and attributes
            //Arrange
            string name = "Mage1";
            Mage newHero = new Mage(name);
            string expected = name;
            //Act
            string actual = newHero.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_CreateRangerWithName_ShouldReturnCorrectName() {
            //Should test for name, level and attributes
            //Arrange
            string name = "Ranger1";
            Ranger newHero = new Ranger(name);
            string expected = name;
            //Act
            string actual = newHero.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_CreateRogueWithName_ShouldReturnCorrectName() {
            //Should test for name, level and attributes
            //Arrange
            string name = "Rogue1";
            Rogue newHero = new Rogue(name);
            string expected = name;
            //Act
            string actual = newHero.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_CreateWarriorWithName_ShouldReturnCorrectName() {
            //Should test for name, level and attributes
            //Arrange
            string name = "Warrior1";
            Warrior newHero = new Warrior(name);
            string expected = name;
            //Act
            string actual = newHero.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_CreateMageCheckLevel_ShouldReturnCorrectNameLevelAndAttributes() {
            //Should test for name, level and attributes
            //Arrange
            int level = 1;
            Warrior newHero = new Warrior("Warrior1");
            int expected = level;
            //Act
            int actual = newHero.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_CreateMageCheckAttributes_ShouldReturnCorrectNameLevelAndAttributes() {
            //Should test for name, level and attributes
            //Arrange
            HeroAttribute mageAttribute = new HeroAttribute(1, 1, 8);
            Mage newHero = new Mage("Mage1");
            HeroAttribute expected = mageAttribute;
            //Act
            HeroAttribute actual = newHero.LevelAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void LevelUp_IncreaseLevelOnMage_ShouldReturnCorrectAmountForLevel() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            int expected = (mage1.Level + 1);
            //Act
            mage1.LevelUp();
            int actual = mage1.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_IncreaseAttributesOnMage_ShouldReturnCorrectAmountForAttributes() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            HeroAttribute heroAttribute = new HeroAttribute(1, 1, 8);
            heroAttribute.increaseAttributes(1, 1, 5);
            HeroAttribute expected = heroAttribute;
            //Act
            mage1.LevelUp();
            HeroAttribute actual = mage1.LevelAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void EquipWeapon_CheckWhenWorks_ShouldReturnKeyValuePairWithWeaponObject() {
            //Arrange 
            Mage mage1 = new Mage("Mage1");
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 2);
            //Act
            var exception = Record.Exception(() => mage1.EquipWeapon(staff));
            //Assert
            Assert.Null(exception);
        }

        [Fact]
        public void EquipWeapon_CheckForExceptions_ShouldReturnInvalidWeaponException() {
            //Arrange 
            Mage mage1 = new Mage("Mage1");
            Weapon staff = new Weapon("Cool Staff", 2, Slots.Weapon, WeaponTypes.Staff, 2);
            //Act
            //Assert
            Assert.Throws<InvalidWeaponException>(() => mage1.EquipWeapon(staff));            
        }

        [Fact]
        public void EquipArmor_EquipArmorOnMage_ShouldReturnKeyValuePairWithArmorObject() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Armor bodyArmor = new Armor("Bodyarmor", 1, Slots.Body, ArmorTypes.Cloth, 1, 0, 0);
            //Act
            var exception = Record.Exception(() => mage1.EquipArmor(bodyArmor));
            //Assert
            Assert.Null(exception);
        }

        [Fact]
        public void EquipArmor_CheckForExceptions_ShouldReturnInvalidArmorException() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Armor bodyArmor = new Armor("Bodyarmor", 1, Slots.Body, ArmorTypes.Plate, 1, 0, 0);
            //Act
            //Assert
            Assert.Throws<InvalidArmorException>(() => mage1.EquipArmor(bodyArmor));
        }

        [Fact]
        public void TotalAttributes_WithNoEquipment_ShouldReturnCorrectTotalAttributes() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            HeroAttribute expected = new HeroAttribute(1, 1, 8);
            //Act
            HeroAttribute actual = mage1.TotalAttributes();
            //Assert
            actual.Should().BeEquivalentTo(expected);

        }

        [Fact]
        public void TotalAttributes_WithOnePieceOfArmor_ShouldReturnCorrectTotalAttributes() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Armor bodyarmor = new Armor("Bodyarmor", 1, Slots.Head, ArmorTypes.Cloth, 1, 0, 0);
            mage1.EquipArmor(bodyarmor);
            HeroAttribute expected = new HeroAttribute(2, 1, 8);
            //Act
            HeroAttribute actual = mage1.TotalAttributes();
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void TotalAttributes_WithTwoPiecesOfArmor_ShouldReturnCorrectTotalAttributes() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Armor bodyarmor = new Armor("Bodyarmor", 1, Slots.Body, ArmorTypes.Cloth, 1, 0, 0);
            Armor headArmor = new Armor("Headarmor", 1, Slots.Head, ArmorTypes.Cloth, 2, 0, 0);
            mage1.EquipArmor(bodyarmor);
            mage1.EquipArmor(headArmor);
            HeroAttribute expected = new HeroAttribute(4, 1, 8);
            //Act
            HeroAttribute actual = mage1.TotalAttributes();
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void TotalAttributes_WithAReplacedPieceOfArmor_ShouldReturnCorrectTotalAttributes() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Armor bodyarmor = new Armor("Bodyarmor", 1, Slots.Body, ArmorTypes.Cloth, 1, 0, 0);
            Armor newBodyArmor = new Armor("NewBodyArmor", 1, Slots.Body, ArmorTypes.Cloth, 2, 0, 0);
            mage1.EquipArmor(bodyarmor);
            mage1.EquipArmor(newBodyArmor);
            HeroAttribute expected = new HeroAttribute(3, 1, 8);
            //Act
            HeroAttribute actual = mage1.TotalAttributes();
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void HeroDamage_WithNoEquipment_ShouldReturnCorrectCalculatedDamage() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            double expected = 1 * (1 + (mage1.TotalAttributes().Intelligence / 100D));
            //Act
            double actual = mage1.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HeroDamage_WithWeaponEquipped_ShouldReturnCorrectCalculatedDamage() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            mage1.EquipWeapon(staff);
            double expected = staff.WeaponDamage * (1 + (mage1.TotalAttributes().Intelligence / 100D));
            //Act
            double actual = mage1.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HeroDamage_WithReplacedWeaponEquipped_ShouldReturnCorrectCalculatedDamage() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            Weapon wand = new Weapon("Fancy Wand", 1, Slots.Weapon, WeaponTypes.Wand, 4);
            mage1.EquipWeapon(staff);
            mage1.EquipWeapon(wand);
            double expected = wand.WeaponDamage * (1 + (mage1.TotalAttributes().Intelligence / 100D));
            //Act
            double actual = mage1.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void HeroDamage_WithWeaponAndArmorEquipped_ShouldReturnCorrectCalculatedDamage() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            Armor bodyArmor = new Armor("BodyArmor", 1, Slots.Body, ArmorTypes.Cloth, 3, 1, 0);
            mage1.EquipWeapon(staff);
            mage1.EquipArmor(bodyArmor);
            double expected = staff.WeaponDamage * (1 + (mage1.TotalAttributes().Intelligence / 100D));
            //Act
            double actual = mage1.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Display_DisplayInfoAboutHero_ShouldReturnNameClassLevelTotalStrengthTotalDexterityTotalIntelligenceAndDamage() {
            //Arrange
            Mage mage1 = new Mage("Mage1");
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Name: Mage1");
            sb.AppendLine("Herotype: Mage");
            sb.AppendLine("Level: 1");
            sb.AppendLine("Total Attributes: \n- Strength: 1\n- Dexterity: 1\n- Intelligence: 8");
            sb.AppendLine("Damage: 1,08");
            string expected = sb.ToString();
            //Act
            string actual = mage1.Display();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}