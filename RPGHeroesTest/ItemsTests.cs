﻿using Xunit;
using FluentAssertions;
using rpgheroes.Attribute;
using rpgheroes.Equipment.Weapons;
using rpgheroes.Equipment.Armors;
using rpgheroes.Equipment.Items;

namespace RPGHeroesTest {
    public class ItemsTests {
        [Fact]
        public void CreateWeapon_CheckForName_ShouldReturnCorrectName() {
            //Arrange
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            string expected = "Cool Staff";
            //Act
            string actual = staff.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWeapon_CheckForLevel_ShouldReturnCorrectLevel() {
            //Arrange
            Weapon staff = new Weapon("Cool Staff", 2, Slots.Weapon, WeaponTypes.Staff, 3);
            int expected = 2;
            //Act
            int actual = staff.RequiredLevel;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWeapon_CheckForSlot_ShouldReturnCorrectSlot() {
            //Arrange
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            Slots expected = Slots.Weapon;
            //Act
            Slots actual = staff.Slot;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWeapon_CheckForWeaponType_ShouldReturnCorrectWeaponType() {
            //Arrange
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            WeaponTypes expected = WeaponTypes.Staff;
            //Act
            WeaponTypes actual = staff.TypeOfWeapon;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateWeapon_CheckForDamage_ShouldReturnCorrectDamage() {
            //Arrange
            Weapon staff = new Weapon("Cool Staff", 1, Slots.Weapon, WeaponTypes.Staff, 3);
            int expected = 3;
            //Act
            int actual = staff.WeaponDamage;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateArmor_CheckForName_ShouldReturnCorrectName() {
            //Arrange
            Armor bodyArmor = new Armor("Bodyarmor", 4, Slots.Body, ArmorTypes.Leather, 4, 1, 0);
            string expected = "Bodyarmor";
            //Act
            string actual = bodyArmor.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateArmor_CheckForRequiredLevel_ShouldReturnCorrectRequiredLevel() {
            //Arrange
            Armor bodyArmor = new Armor("Bodyarmor", 4, Slots.Body, ArmorTypes.Leather, 4, 1, 0);
            int expected = 4;
            //Act
            int actual = bodyArmor.RequiredLevel;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateArmor_CheckForSlot_ShouldReturnCorrectSlot() {
            //Arrange
            Armor bodyArmor = new Armor("Bodyarmor", 4, Slots.Body, ArmorTypes.Leather, 4, 1, 0);
            Slots expected = Slots.Body;
            //Act
            Slots actual = bodyArmor.Slot;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateArmor_CheckForArmorType_ShouldReturnCorrectArmorType() {
            //Arrange
            Armor bodyArmor = new Armor("Bodyarmor", 4, Slots.Body, ArmorTypes.Leather, 4, 1, 0);
            ArmorTypes expected = ArmorTypes.Leather;
            //Act
            ArmorTypes actual = bodyArmor.TypeOfArmor;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateArmor_CheckForArmorAttributes_ShouldReturnCorrectArmorAttributes() {
            //Arrange
            Armor bodyArmor = new Armor("Bodyarmor", 4, Slots.Body, ArmorTypes.Leather, 4, 1, 0);
            HeroAttribute expected = new HeroAttribute(4, 1, 0);
            //Act
            HeroAttribute actual = bodyArmor.ArmorAttribute;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }
    }
}
